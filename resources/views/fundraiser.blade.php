@extends('master', ['title' => 'Fundraiser Reviewer - '.$fundraiser->name])

@section('content')

<div id="app">
    <nav-bar title="{{ $fundraiser->name }}"></nav-bar>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <review-list :fundraiser-id="{{ $fundraiser->id }}"></review-list>
            </div>
        </div>
    </div>
    <create-review-modal :fundraiser-id="{{ $fundraiser->id }}"></create-review-modal>
</div>

@section('bottom-scripts')

<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

@endsection

@endsection
