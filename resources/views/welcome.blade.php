@extends('master', ['title' => 'Fundraiser Reviewer'])

@section('content')

<div id="app">
    <nav-bar title="Fundraiser Reviewer"></nav-bar>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <fundraiser-list search-term=""></fundraiser-list>
            </div>
        </div>
    </div>

    <create-fundraiser-modal></create-fundraiser-modal>
</div>

@section('bottom-scripts')

<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

@endsection

@endsection
