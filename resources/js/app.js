/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('fundraiser-list', require('./components/FundraiserList.vue'));
Vue.component('fundraiser', require('./components/Fundraiser.vue'));
Vue.component('fundraiser-not-found', require('./components/FundraiserNotFound.vue'));
Vue.component('text-input', require('./components/TextInput.vue'));
Vue.component('text-area', require('./components/TextArea.vue'));
Vue.component('primary-button', require('./components/PrimaryButton.vue'));
Vue.component('modal', require('./components/Modal.vue'));
Vue.component('create-fundraiser-modal', require('./components/CreateFundraiserModal.vue'));
Vue.component('review-list', require('./components/ReviewList.vue'));
Vue.component('review', require('./components/Review.vue'));
Vue.component('create-review-modal', require('./components/CreateReviewModal.vue'));
Vue.component('rating', require('./components/Rating.vue'));
Vue.component('nav-bar', require('./components/NavBar.vue'));

const app = new Vue({
    el: '#app'
});
