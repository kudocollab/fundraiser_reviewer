<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Fundraiser;
use App\Models\Review;
use Illuminate\Support\Facades\DB;

class FundraisersController extends Controller
{
    public function getAllFundraisers()
    {
        return Fundraiser::orderBy('rating', 'desc')->get();
    }

    public function createFundraiser()
    {
        if (Fundraiser::where('name', 'like', request('name'))->count()) {
            abort(409, 'Fundraiser already exists');
        }

        $new_fundraiser = new Fundraiser;
        $new_fundraiser->name = request('name');
        $new_fundraiser->description = request('description');
        $new_fundraiser->save();

        return self::getAllFundraisers();
    }

    public function getFundraiserReviews($fundraiser_id)
    {
        return Review::where('fundraiser_id', $fundraiser_id)->get();
    }

    public function createReview($fundraiser_id)
    {
        if (Review::where([['fundraiser_id', $fundraiser_id], ['reviewer_email', 'like', request('email')]])->count()) {
            abort(409, 'Review with user already exists');
        }
        
        DB::transaction(function () use ($fundraiser_id) {
            $new_review = new Review;
            $new_review->title = request('title');
            $new_review->reviewer_email = request('email');
            $new_review->content = request('review');
            $new_review->rating = request('rating');
            $new_review->fundraiser_id = $fundraiser_id;
            $new_review->save();

            $fundraiser = Fundraiser::find($fundraiser_id);
            $fundraiser->rating = Review::where('fundraiser_id', $fundraiser_id)->sum('rating') / Review::where('fundraiser_id', $fundraiser_id)->count();
            $fundraiser->save();
        });
        
        return self::getFundraiserReviews($fundraiser_id);
    }
}
