<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Fundraiser;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/fundraiser/{fundraiser_id}', function ($fundraiser_id) {
    $fundraiser = Fundraiser::find($fundraiser_id);
    
    return view('fundraiser', ['fundraiser' => $fundraiser]);
});

Route::get('/fundraisers', 'FundraisersController@getAllFundraisers');
Route::put('/fundraiser', 'FundraisersController@createFundraiser');

Route::get('/reviews/{fundraiser_id}', 'FundraisersController@getFundraiserReviews');
Route::put('/reviews/{fundraiser_id}', 'FundraisersController@createReview');
